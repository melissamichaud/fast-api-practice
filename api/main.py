from fastapi import FastAPI
from routers import vacations


app = FastAPI()
# Import router from vacations.py -> hook up to main app
app.include_router(vacations.router)
